package collections;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Runner {

	public static void main(String[] args) {
		File file = new File("src\\collections\\collections.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		List<String> collectionInterfaces = new ArrayList<>();
		List<String> collectionsClasses = new ArrayList<>();

		try {
			DocumentBuilder db = dbf.newDocumentBuilder();
			try {
				Document document = db.parse(file);

				String rootElement = document.getDocumentElement().getNodeName();
				System.out.println(rootElement);
				NodeList nodeList = document.getElementsByTagName("interface");

				for (int i = 0; i < nodeList.getLength(); i++) {
					Node node = nodeList.item(i);
					Element element = (Element) node;
					// interface
					System.out.println("interface: " + element.getElementsByTagName("name").item(0).getTextContent());

					int k = 0;
					while (element.getElementsByTagName("class").item(k) != null) {

						System.out
								.println("\tclass: " + element.getElementsByTagName("class").item(k).getTextContent());
						k++;
					}

				}
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
