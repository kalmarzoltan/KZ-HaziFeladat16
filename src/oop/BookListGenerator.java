package oop;
import java.util.ArrayList;
import java.util.List;

public class BookListGenerator implements IBooks {

	@Override
	public List<Book> prepareBooklist(int pcs) {
		List<Book> books = new ArrayList<>();

		for (int i = 0; i < pcs; i++) {
			Book book = new Book("title" + i, "author" + i, 00 + i);
			books.add(book);
		}

		return books;
	}

}
