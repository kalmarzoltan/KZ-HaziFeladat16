package oop;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Book {

	private String title;
	private String author;
	private double isbn;

	public Book(String title, String author, double isbn) {
		this.title = title;
		this.author = author;
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public double getIsbn() {
		return isbn;
	}

	public void setIsbn(double isbn) {
		this.isbn = isbn;
	}

	@Override
	public String toString() {
		NumberFormat formatter = new DecimalFormat("#0");
		return "title=" + title + ", author=" + author + ", isbn=" + formatter.format(isbn) + "";
	}

}
