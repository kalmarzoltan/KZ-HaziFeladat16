package oop;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlHelper {
	void prepareTheXml(List<Book> bookList) {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = dbf.newDocumentBuilder();

			Document doc = builder.newDocument();

			Element rootElement = doc.createElement("books");
			doc.appendChild(rootElement);

			for (int i = 0; i < bookList.size(); i++) {

				Element book = doc.createElement("book");
				rootElement.appendChild(book);

				Attr attr = doc.createAttribute("id");
				attr.setValue("00" + i);
				book.setAttributeNode(attr);

				Element title = doc.createElement("title");
				title.appendChild(doc.createTextNode(bookList.get(i).getTitle()));
				book.appendChild(title);

				Element author = doc.createElement("author");
				author.appendChild(doc.createTextNode(bookList.get(i).getAuthor()));
				book.appendChild(author);

				Element isbn = doc.createElement("isbn");

				isbn.appendChild(doc.createTextNode(String.valueOf(bookList.get(i).getIsbn())));
				book.appendChild(isbn);

			}

			TransformerFactory trf = TransformerFactory.newInstance();
			Transformer transformer = trf.newTransformer();

			DOMSource domSource = new DOMSource(rootElement);

			StreamResult str = new StreamResult(new File("src\\oop\\output-oop.xml"));

			try {

				transformer.transform(domSource, str);

			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println("XML letrehozva");

		} catch (ParserConfigurationException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
