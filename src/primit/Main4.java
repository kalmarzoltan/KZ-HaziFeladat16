package primit;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Main4 {

	public static void main(String[] args) {

		try {
			DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = builderFactory.newDocumentBuilder();

			Document doc = builder.newDocument();

			// books
			Element rootElement = doc.createElement("books");
			doc.appendChild(rootElement);

			String[][] tomb = new String[4][3];
			tomb[0][0] = "Clean Code";
			tomb[0][1] = "Robert C. Martin";
			tomb[0][2] = "111";

			tomb[1][0] = "The Clean Coder: A Code of Conduct for Professional Programmerse";
			tomb[1][1] = "Robert C. Martin";
			tomb[1][2] = "222";

			tomb[2][0] = "A C programozasi nyelv";
			tomb[2][1] = "BRIAN W. KERNIGHAN - DENNIS M. RITCHIE";
			tomb[2][2] = "333";

			String[] tagNames = new String[] { "title", "author", "isbn" };

			tomb[3][0] = tagNames[0];
			tomb[3][1] = tagNames[1];
			tomb[3][2] = tagNames[2];

			for (int i = 0; i <= 2; i++) {
				// book
				Element book = doc.createElement("book");
				rootElement.appendChild(book);

				// book add attribute
				Attr attr = doc.createAttribute("id");
				attr.setValue("0" + (i + 1));
				book.setAttributeNode(attr);

				// title
				Element title = doc.createElement(tomb[3][0]);
				title.appendChild(doc.createTextNode(tomb[i][0]));
				book.appendChild(title);

				// author
				Element author = doc.createElement(tomb[3][1]);
				author.appendChild(doc.createTextNode(tomb[i][1]));
				book.appendChild(author);

				// isbn
				Element isbn = doc.createElement(tomb[3][2]);
				isbn.appendChild(doc.createTextNode(tomb[i][2]));
				book.appendChild(isbn);

			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();

			DOMSource domSource = new DOMSource(rootElement);
			StreamResult streamResult = new StreamResult(new File("src\\primit\\output-Main4.xml"));

			transformer.transform(domSource, streamResult);

			System.out.println("XML Letrehozva.");

		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
