1) "primitív" megoldás (fapad, de fontos)
A Main4.java file-ban elkészített algoritmus optimalizálása: Úgy, hogy a legkevesebb lépésből végrehajtható legyen.
Ehhez az adatok megfelelő strukturálása elengedhetetlen.
Javaslatok: (de ha van ügyesebb ötlet, akkor bátran)
   - egy kétdimenziós tömb készítése a bemenő adatoknak(ez megvan), majd egy egydimenziós a tag neveinek
   - a két dimenziós tomb átalakítása, hogy tárolni tudja a tag-ek neveit is

(ez történik minden esetben, csak pl egy magasabb szinten)

2) OOP megoldás
A Main4.java file-ban elkészített algoritmus optimalizálása Collections használatával.
Itt fel kell mérni, hogy a java-ban milyen collection-k vannak (ezt tanultuk), ezután melyiket érdemes használni ezen adatok tárolására.

(ez pl a magasabb szint)

3)
Ha a fentiekkel elkészültök, akkor készítsetek egy xml struktúrát a java Collections osztályból származtatott osztályok tárolására, majd olvassátok be java-ba az xml-t.
